# Run project locally

Clone Project from Gitlab

## `git clone git@gitlab.com:r1106/rcg-essantials-basics.git`

# Installation

Install missing packages in the root folder

## `npm install`

# Start project in development mode

In the project directory, you can run:

## `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes. You may also see any lint errors in the console.

# Pages Overview

![Components Screen](src/images/readme/react_basics_01.png "Components Screen")

![JSX Screen](src/images/readme/react_basics_02.png "JSX Screen")

![Props Screen](src/images/readme/react_basics_03.png "Props Screen")

![State Screen](src/images/readme/react_basics_04.png "State Screen")

