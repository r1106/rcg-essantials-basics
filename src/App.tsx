import "./App.scss";
import CoreConcepts from "./components/core-concepts/CoreConcepts";
import Examples from "./components/examples/Examples";
import "./components/header/Header";
import Header from "./components/header/Header";

function App(): JSX.Element {
  return (
    <>
      <Header></Header>
      <main>
        <CoreConcepts></CoreConcepts>
        <Examples></Examples>
      </main>
    </>
  );
}

export default App;
