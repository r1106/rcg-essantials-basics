import componetsImage from "../../assets/components.png";
import propsImage from "../../assets/config.png";
import jsxImage from "../../assets/jsx-ui.png";
import stateImage from "../../assets/state-mgmt.png";
import { ICoreConcept } from "../interfaces/core-concept.interface";

export const CORE_CONCEPTS: ICoreConcept[] = [
  {
    title: "Components",
    desc: "The core UI building block - compose the user interface by combining multiple componets.",
    image: { path: componetsImage },
  },
  {
    title: "JSX",
    desc: "Return (potentially dynamic) HTML(ish) code to define the actual markup that will be rendered.",
    image: { path: jsxImage },
  },
  {
    title: "Props",
    desc: "Make componets configurable (and therefore reusable) by passing input data to them.",
    image: { path: propsImage },
  },
  {
    title: "State",
    desc: "React-managed data which, when changed, causes the component to re-render & the UI to update.",
    image: { path: stateImage },
  },
];
