import { IExample } from "../interfaces/example.interface";

export const EXAMPLES: IExample = {
  components: {
    title: "Components",
    desc: "Components are the building blocks of React applications. A Component is a self-contained module (HTML + optional CSS + JS) that renders some output.",
    code: `
    function Welcome() {
      return <h1>Hello, World!</h1>;
    }
    `,
  },
  jsx: {
    title: "JSX",
    desc: "JSX is a syntax extension to JavaScript. It is similar to a template, but it has full power of JavaScript (e.g., it may output dynamix content).",
    code: `
      <div>
        <h1>Welcome {userName}</h1>
        <p>Time to learn React!</p>
      </div>
    `,
  },
  props: {
    title: "Props",
    desc: "Components accepts arbitrary inputs called props. They are like function arguments",
    code: `
      function Welcome(props) {
        return <h1>Hello, {props.name}</h1>;
      }
    `,
  },
  state: {
    title: "State",
    desc: "State allows React components to change their output over time in response to user actions, network reponses, and anything else.",
    code: `
      function Counter() {
        const [isVisible, setIsVisible] = useState(false);

        function handleClick() {
          setIsVisible(true);
        }

        return (
          <div>
            <button onClick={handleClick}>Show Details</button>
            {isVisible && <p>Amazing details!</p>}
          </div>
        );
      }
    `,
  },
};
