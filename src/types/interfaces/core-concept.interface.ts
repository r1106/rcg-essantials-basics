export interface ICoreConcept {
  title: string;
  desc: string;
  image: { path: string };
}
