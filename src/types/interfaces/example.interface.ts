import { IExampleSubKeys } from "./example-sub-keys.interface";

export interface IExample {
  components: IExampleSubKeys;
  jsx: IExampleSubKeys;
  props: IExampleSubKeys;
  state: IExampleSubKeys;
}
