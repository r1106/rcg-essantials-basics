export interface IExampleSubKeys {
  title: string;
  desc: string;
  code: string;
}
