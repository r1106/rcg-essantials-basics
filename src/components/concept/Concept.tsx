import { ICoreConcept } from "../../types/interfaces/core-concept.interface";

function Concept({ title, desc, image }: ICoreConcept): JSX.Element {
  return (
    <li>
      <img src={image.path} alt="logo"></img>
      <h3>{title}</h3>
      <p>{desc}</p>
    </li>
  );
}

export default Concept;
