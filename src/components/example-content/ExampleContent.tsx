import { IExampleSubKeys } from "../../types/interfaces/example-sub-keys.interface";

function ExampleContent({ title, desc, code }: IExampleSubKeys): JSX.Element {
  return (
    <div id="tab-content">
      <h3>{title}</h3>
      <p>{desc}</p>
      <pre>{code}</pre>
    </div>
  );
}

export default ExampleContent;
