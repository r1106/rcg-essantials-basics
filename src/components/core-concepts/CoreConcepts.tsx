import { CORE_CONCEPTS } from "../../types/consts/core-concepts.const";
import Concept from "../concept/Concept";

function CoreConcepts(): JSX.Element {
  return (
    <div id="core-concepts">
      <h2>Core Concepts</h2>
      <ul>
        {CORE_CONCEPTS.map((item) => (
          <Concept key={item.title} {...item}></Concept>
        ))}
      </ul>
    </div>
  );
}

export default CoreConcepts;
