import { MouseEventHandler } from "react";

function ExampleButton({
  title,
  isSelected,
  onClick = undefined,
}: {
  title: string;
  isSelected: boolean;
  onClick: MouseEventHandler | undefined;
}): JSX.Element {
  return (
    <button className={isSelected ? "active" : ""} onClick={onClick}>
      {title}
    </button>
  );
}

export default ExampleButton;
