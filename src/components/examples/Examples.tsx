import { Dispatch, SetStateAction, useState } from "react";
import { EXAMPLES } from "../../types/consts/examples.const";
import ExampleButton from "../example-button/ExampleButton";
import ExampleContent from "../example-content/ExampleContent";

function Examples(): JSX.Element {
  const [selectedExample, setSelectedExample]: [
    string,
    Dispatch<SetStateAction<string>>
  ] = useState(EXAMPLES.components.title.toLocaleLowerCase());

  function onClick(title: string): void {
    setSelectedExample(title);
  }

  return (
    <section id="examples">
      <h2>Examples</h2>
      <menu>
        {Object.keys(EXAMPLES).map((key) => {
          const value = EXAMPLES[key as keyof typeof EXAMPLES];
          return (
            <ExampleButton
              key={value.title + Math}
              title={value.title}
              isSelected={key === selectedExample}
              onClick={() => onClick(key)}
            ></ExampleButton>
          );
        })}
      </menu>
      <ExampleContent
        {...EXAMPLES[selectedExample as keyof typeof EXAMPLES]}
      ></ExampleContent>
    </section>
  );
}

export default Examples;
