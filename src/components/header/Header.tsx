import logo from "../../assets/react-core-concepts.png";

function Header(): JSX.Element {
  return (
    <header>
      <img src={logo} alt="logo" />
      <h1>React Essentials</h1>
      <p>
        Core React concept you will need for almost any app you are going to
        build!
      </p>
    </header>
  );
}

export default Header;
